import { describe, it, expect } from 'vitest';
import { add } from './kata-2';

describe('String Calculator', () => {
  it('should return 0 for an empty string', () => {
    expect(add("")).toBe(0);
  });

  it('should return the number itself when only one number is provided', () => {
    expect(add("1")).toBe(1);
    expect(add("5")).toBe(5);
  });

  it('should return the sum of two numbers separated by a comma', () => {
    expect(add("1,2")).toBe(3);
    expect(add("10,20")).toBe(30);
  });

  it('should return the sum of an unknown number of arguments', () => {
    expect(add("1,2,3")).toBe(6);
    expect(add("4,5,6,7")).toBe(22);
  });

  it('should handle newlines as separators', () => {
    expect(add("1,2\n3")).toBe(6);
    expect(add("1\n2,3")).toBe(6);
  });

  it('should throw an error if there is a separator at the end', () => {
    expect(() => add("1,2,")).toThrowError("Invalid input");
  });

  it('should handle different delimiters', () => {
    expect(add("//;\n1;2")).toBe(3);
    expect(add("//|\n1|2|3")).toBe(6);
    expect(add("//sep\n2sep5")).toBe(7);
  });

});

