export function add(numbers: string): number {
  if (numbers === "") return 0;

  let delimiter = /[,|\n]/;
  let numberString = numbers;

  if (numbers.startsWith("//")) {
    const delimiterEndIndex = numbers.indexOf("\n");
    delimiter = new RegExp(numbers.substring(2, delimiterEndIndex));
    numberString = numbers.substring(delimiterEndIndex + 1);
  }

  if (numberString.endsWith(",") || numberString.endsWith("\n")) {
    throw new Error("Invalid input");
  }

  const numArray = numberString.split(delimiter).map(num => {
    if (num.trim() === '') return 0;
    return parseInt(num, 10);
  });
  return numArray.filter(d => !isNaN(d)).reduce((sum, num) => sum + num, 0);
}
