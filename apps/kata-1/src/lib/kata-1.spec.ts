import { describe, it, expect } from 'vitest';
import { fizzBuzz } from './kata-1';

describe('fizzBuzz', () => {
  it('should return the number as a string', () => {
    expect(fizzBuzz(1)).toBe('1');
    expect(fizzBuzz(2)).toBe('2');
  });

  it('should return "Fizz" for multiples of three', () => {
    expect(fizzBuzz(3)).toBe('Fizz');
    expect(fizzBuzz(6)).toBe('Fizz');
  });

  it('should return "Buzz" for multiples of five', () => {
    expect(fizzBuzz(5)).toBe('Buzz');
    expect(fizzBuzz(10)).toBe('Buzz');
  });

  it('should return "FizzBuzz" for multiples of both three and five', () => {
    expect(fizzBuzz(15)).toBe('FizzBuzz');
    expect(fizzBuzz(30)).toBe('FizzBuzz');
  });
});



